## How does it work? 
1. Put subtitles files in **Subtitles** folder
2. Open **Source** folder and run **Source.exe**
3. There are the result files in **Generated Words** folder

## Files: 
**Oxford Base Words** is a list of the 3000 most important words to learn in English

**Most Popular Words** is 100 or fewer words from subtitles that contain **Oxford Base Words**

**New Words From Subtitles** is words from subtitles files

**PersonalDictionary** is your learned words that won't appear in **Oxford Base Words** and **New Words From Subtitles** files
 
